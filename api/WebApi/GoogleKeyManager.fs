﻿namespace WebApi

open System
open System.Linq
open RestSharp
open Microsoft.IdentityModel.Tokens
open Newtonsoft.Json
open Newtonsoft.Json.Linq

type GoogleKeyManager() =

  static let client = new RestClient("https://www.googleapis.com/oauth2/v3/certs")
  static let req = new RestRequest()

  static let mutable cachedKeys: Option<seq<SecurityKey>> = None
  static let mutable expires: Option<DateTime> = None

  static let getCached() =
    match expires with
    | Some e -> if e <= DateTime.Now then None else cachedKeys
    | None -> None

  static let setExpires (res : IRestResponse) =
    let cacheControlHeader = res.Headers.First(fun h -> h.Name = "Cache-Control").Value.ToString()
    let cacheControlValue = System.Net.Http.Headers.CacheControlHeaderValue.Parse(cacheControlHeader)
    expires <- Some (cacheControlValue.MaxAge.Value |> DateTime.Now.Add)

  static let getFromUrl() =
  
    let res = client.Execute(req)

    res |> setExpires

    res.Content

  static let parseResponse (res: string) =
    let parsed = JsonConvert.DeserializeObject<JObject>(res)
    parsed.Value<JArray>("keys") |> Seq.map (fun key -> new JsonWebKey(key.ToString()) :> SecurityKey)

  static member getKeys() =
    match getCached() with
    | Some c ->
      "Using cached key..." |> Console.WriteLine
      c
    | None ->
      "Getting new key..." |> Console.WriteLine
      let res = getFromUrl()
      let parsed = res |> parseResponse
      cachedKeys <- Some parsed
      parsed