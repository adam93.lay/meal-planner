﻿namespace WebApi.Controllers

open System.Collections.Generic
open System.Linq
open Microsoft.AspNetCore.Mvc
open WebApi.Database
open Microsoft.Extensions.Configuration
open Microsoft.EntityFrameworkCore
open Microsoft.AspNetCore.Mvc
open Microsoft.AspNetCore.Authorization

[<Route("api/[controller]")>]
[<Authorize>]
[<ApiController>]
type ItemsController (db : MealPlannerContext, config : IConfiguration) =
  inherit ControllerBase()

  [<HttpGet>]
  member __.Get() =
    
    db.Items |> ActionResult<IEnumerable<Item>>

  [<HttpGet("{id}")>]
  member __.Get(id:int) =

    db.Items.FirstOrDefault(fun item -> item.Id = id) |> ActionResult<Item>

  [<HttpPost>]
  member this.Post( [<FromBody>] value : Item) =
    // Add the new item to db
    value |> db.Items.Add |> ignore
    // Save changes
    db.SaveChanges() |> ignore
    // Send back the new item ID
    value.Id |> ActionResult<int>

  [<HttpPut("{id}")>]
  member this.Put( id : int, [<FromBody>] value : Item ) =
    // Attach the item to db
    value |> db.Items.Attach |> ignore
    // Set db item state to modified
    db.Entry(value).State <- EntityState.Modified
    // Save changes!
    db.SaveChanges() |> ignore
    // Echo back the item
    value |> ActionResult<Item>

  [<HttpDelete("{id}")>]
  member this.Delete(id:int) =
    // Remove single item from db
    db.Items.Single(fun i -> i.Id = id) |> db.Items.Remove |> ignore
    //Save changes
    db.SaveChanges() |> ignore
    // Echo back the deleted id
    id |> ActionResult<int>
