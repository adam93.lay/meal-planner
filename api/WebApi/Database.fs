﻿namespace WebApi.Database

open Microsoft.EntityFrameworkCore
open System.Diagnostics

type [<CLIMutable>] Item = {
  Id: int
  Name: string
}

type MealPlannerContext(options : DbContextOptions<MealPlannerContext>) =
  inherit DbContext(options :> DbContextOptions)

    new() = new MealPlannerContext(new DbContextOptions<MealPlannerContext>())

    override this.OnModelCreating (modelBuilder : ModelBuilder) =

      this.ConfigureItem |> modelBuilder.Entity<Item> |> ignore

    member __.ConfigureItem (entity) =
      entity.ToTable("items") |> ignore

      entity.Property(fun e -> e.Id).HasColumnName("id") |> ignore

      entity
        .Property(fun e -> e.Name)
        .IsRequired()
        .HasColumnName("name") |> ignore
    
    
    [<DefaultValue>] val mutable items: DbSet<Item>
    member this.Items with get() = this.items and set i = this.items <- i