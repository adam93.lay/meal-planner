namespace WebApi

open System
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.DependencyInjection
open Microsoft.EntityFrameworkCore
open WebApi.Database
open Microsoft.AspNetCore.Authentication.JwtBearer
open Microsoft.IdentityModel.Tokens
open System.Threading.Tasks

type Startup private () =
  new (configuration: IConfiguration) as this =
    Startup() then
    this.Configuration <- configuration

  // This method gets called by the runtime. Use this method to add services to the container.
  member this.ConfigureServices(services: IServiceCollection) =

    services
      .AddAuthentication(fun options ->
        options.DefaultAuthenticateScheme <- JwtBearerDefaults.AuthenticationScheme
        options.DefaultChallengeScheme <- JwtBearerDefaults.AuthenticationScheme
      )
      .AddJwtBearer(fun options ->
        
        options.RequireHttpsMetadata <- false

        options.TokenValidationParameters <- new TokenValidationParameters()
        
        options.TokenValidationParameters.ValidateLifetime <- false

        options.TokenValidationParameters.ValidateIssuerSigningKey <- true
        options.TokenValidationParameters.IssuerSigningKeys <- GoogleKeyManager.getKeys()
        
        options.TokenValidationParameters.ValidIssuers <- [ "accounts.google.com" ; "https://accounts.google.com" ]
        options.TokenValidationParameters.ValidateIssuer <- true
        
        options.Audience <- "317540764378-fvre24u1e6581fuo5fqcqvf1khpo2d0b.apps.googleusercontent.com"
        options.TokenValidationParameters.ValidateAudience <- true

        options.Events <- new JwtBearerEvents()

        options.Events.OnMessageReceived <- (fun (ctx:MessageReceivedContext) ->
          ctx.Options.TokenValidationParameters.IssuerSigningKeys <- GoogleKeyManager.getKeys()
          Task.CompletedTask
        )

      ) |> ignore
    // Add framework services.
    services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2) |> ignore

    // Add configuration
    services.AddSingleton<IConfiguration>(this.Configuration) |> ignore

    let conn = this.Configuration.GetConnectionString("main") // @"Host=172.17.0.3;Database=postgres;Username=postgres;Password=0DCFA546ab;"

    // Add EF (NPGSQL)
    services
      .AddEntityFrameworkNpgsql()
      .AddDbContext<MealPlannerContext>(fun options -> options.UseNpgsql(conn) |> ignore)
      .BuildServiceProvider() |> ignore

  // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
  member this.Configure(app: IApplicationBuilder, env: IHostingEnvironment) =
    if (env.IsDevelopment()) then
      app.UseDeveloperExceptionPage() |> ignore

    app.UseAuthentication() |> ignore

    app.UseCors(fun builder ->
      builder.WithOrigins("*") |> ignore
      builder.WithMethods("*") |> ignore
      builder.WithHeaders("*") |> ignore
    ) |> ignore
    app.UseMvc() |> ignore

  member val Configuration : IConfiguration = null with get, set
