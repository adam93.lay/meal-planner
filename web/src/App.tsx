import React, { Component } from 'react';
import './App.css';
import { Route, BrowserRouter } from 'react-router-dom';
import SignIn from './pages/SignIn';
import HomePage from './pages/HomePage';
import { connect } from 'react-redux';
import { signIn, signOut } from './actions/AuthActions';
import { GlobalState } from './reducers';
import DashboardLayout from './DashboardLayout';
import Splash from './_components/Splash';
import ItemsPage from './pages/ItemsPage';
import history from './history'
import RecipesPage from './pages/RecipesPage';
import MealsPage from './pages/MealsPage';
import KitchenPage from './pages/KitchenPage';

interface AppProps {
  isSignedIn: boolean|null;
  signIn: (token: string) => void;
  signOut: () => void;
}

class App extends Component<AppProps> {

  constructor(props: AppProps) {
    super(props);
  }
  
  componentDidMount() {

    const gapi = (window as any).gapi;
    
    gapi.load('auth2', () => {
      gapi.auth2.init({
        client_id: '317540764378-fvre24u1e6581fuo5fqcqvf1khpo2d0b.apps.googleusercontent.com',
      }).then((auth: any) => {
        const isSignedIn = auth.isSignedIn.get();
        if (isSignedIn) {
          const token = auth.currentUser.get().getAuthResponse().id_token;
          this.props.signIn(token);
        } else {
          this.props.signOut();
        }
      });
    });
    
  }

  render() {
    const { isSignedIn } = this.props;

    const main = (
      <BrowserRouter>
        <DashboardLayout>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/items" component={ItemsPage} />
          <Route exact path="/recipes" component={RecipesPage} />
          <Route exact path="/meals" component={MealsPage} />
          <Route exact path="/kitchen" component={KitchenPage} />
          <Route path="/login" component={SignIn} />
        </DashboardLayout>
      </BrowserRouter>
    );

    if (isSignedIn === null)
      return <Splash />

    return isSignedIn ? main : <SignIn />;
  }
}

const mapStateToProps = (state: GlobalState) => ({
  isSignedIn: state.auth.isSignedIn
});

export default connect(mapStateToProps, { signIn, signOut })(App);
