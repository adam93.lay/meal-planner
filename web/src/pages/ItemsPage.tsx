import React, { Component } from 'react'
import { connect } from 'react-redux';
import { GlobalState } from '../reducers';
import ItemsList from '../_components/items/ItemsList';
import NewItem from '../_components/items/NewItem';

class ItemsPage extends React.Component {

  render() {

    return (
      <div className="home">
        <NewItem />
        <ItemsList />
      </div>
    );
  }
}

export default connect(null, { })(ItemsPage);
