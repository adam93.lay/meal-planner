import React, { Component } from 'react';
import './SignIn.css';
import Paper from '@material-ui/core/Paper';
import { Typography, FormControl, InputLabel, Input, CssBaseline, FormControlLabel, Button, Checkbox, Avatar } from '@material-ui/core';
import  LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { connect } from 'react-redux';
import { signIn } from '../actions/AuthActions';

interface SignInProps {
  signIn: (token: string) => void;
}

interface SignInState {
  email?: string;
  password?: string;
  remember?: boolean;
}

class SignIn extends Component<SignInProps, SignInState> {

  constructor(props: SignInProps) {
    super(props);

    this.state = {
      email: "",
      password: "",
      remember: false
    };

    this.signIn = this.signIn.bind(this);
  }

  componentDidMount() {
    const gapi = (window as any).gapi;
    gapi.signin2.render("btnGoogleSignIn", {
      longtitle: true,
      theme: "dark",
      onsuccess: this.googleSignIn
    });
  }

  handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  googleSignIn = (googleUser: any) => {
    const token = googleUser.getAuthResponse().id_token;
    this.props.signIn(token);
  }

  signIn() {
    
  }

  render() {
    return (
      <main>
        <CssBaseline />
        <Paper className="login-box">
          <Avatar style={{margin:8}}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">Sign in</Typography>
          <form style={{marginTop:8}}>
            <FormControl margin="normal" required fullWidth>
              <InputLabel>Email Address</InputLabel>
              <Input name="email" id="email" autoComplete="email" value={this.state.email} onChange={this.handleInputChange} />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <InputLabel>Password</InputLabel>
              <Input name="password" id="password" type="password" autoComplete="current-password" value={this.state.password} onChange={this.handleInputChange} />
            </FormControl>
            <FormControlLabel
              control={<Checkbox name="remember" value="remember" color="primary" onChange={this.handleInputChange} checked={this.state.remember} />}
              label="Remember me" />
            <Button
              type="submit"
              onClick={this.signIn}
              fullWidth
              variant="contained"
              color="primary"
              style={{marginTop:24}}>
              Sign in
            </Button>
            <div id="btnGoogleSignIn" className="g-signin2"></div>
          </form>
        </Paper>
      </main>
    );
  }
}

export default connect(null, { signIn })(SignIn);
