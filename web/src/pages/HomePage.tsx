import React, { Component } from 'react'
import { connect } from 'react-redux';
import { GlobalState } from '../reducers';

class HomePage extends React.Component {

  render() {

    return (
      <div className="home">
        <p>Home</p>
      </div>
    );
  }
}

export default connect(null, { })(HomePage);
