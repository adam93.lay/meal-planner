import React, { Component } from 'react'
import { connect } from 'react-redux';
import { GlobalState } from '../reducers';

class RecipesPage extends React.Component {

  render() {

    return (
      <div className="recipes">
        <p>Recipes</p>
      </div>
    );
  }
}

export default connect(null, { })(RecipesPage);
