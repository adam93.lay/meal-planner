import React from 'react';
import './DashboardLayout.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import BookIcon from '@material-ui/icons/Book';
import DateRangeIcon from '@material-ui/icons/DateRange';
import KitchenIcon from '@material-ui/icons/Kitchen';
import AccountMenu from './_components/AccountMenu';
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import history from './history'
import { Link } from 'react-router-dom';

export default class DashboardLayout extends React.Component {
  state = {
    open: false,
  };

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  render() {
    //const { classes } = this.props;

    return (
      <div className="root">
        <CssBaseline />

        <AppBar
          position="absolute"
          className={`app-bar ${this.state.open ? "app-bar--shift" : ""}`}>

          <Toolbar disableGutters={!this.state.open} className="toolbar">
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerOpen}
              className={`menu-button ${this.state.open ? "menu-button--hidden" : ""}`}>
              <MenuIcon />
            </IconButton>
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              className="title">
              Dashboard
            </Typography>
            <IconButton color="inherit">
              <Badge badgeContent={4} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            
            <AccountMenu />
          </Toolbar>
        </AppBar>

        <Drawer
          variant="permanent"
          classes={{
            paper: `drawer-paper ${!this.state.open && "drawer-paper--close"}`
          }}
          open={this.state.open}>
          <div className="toolbar-icon">
            <IconButton onClick={this.handleDrawerClose}>
              <ChevronLeftIcon />
            </IconButton>
          </div>
          <Divider />
          <List>
            <Link to="/items">
              <ListItem button>
                <ListItemIcon>
                  <ShoppingCartIcon />
                </ListItemIcon>
                <ListItemText primary="Products" />
              </ListItem>
            </Link>
            <Link to="/recipes">
              <ListItem button>
                <ListItemIcon>
                  <BookIcon />
                </ListItemIcon>
                <ListItemText primary="Recipes" />
              </ListItem>
            </Link>
            <Link to="/meals">
              <ListItem button>
                <ListItemIcon>
                  <DateRangeIcon />
                </ListItemIcon>
                <ListItemText primary="Meal Plans" />
              </ListItem>
            </Link>
            <Link to="/kitchen">
              <ListItem button>
                <ListItemIcon>
                  <KitchenIcon />
                </ListItemIcon>
                <ListItemText primary="Kitchen" />
              </ListItem>
            </Link>
          </List>
          <Divider />
          {/* <List>{secondaryListItems}</List> */}
        </Drawer>

        <main className="content">
          <div className="app-bar--spacer" />
          {this.props.children}
        </main>
      </div>
    );
  }
}
