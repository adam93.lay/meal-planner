import Item from "../models/Item";
import { GET_ITEMS, CREATE_ITEM, GET_ITEM, DELETE_ITEM, UPDATE_ITEM } from "../actions/Types";
import { Action } from ".";

export interface ItemsState {
  items: Item[];
  updatedItem?: Item;
}

const initialState: ItemsState = {
  items: []
};

type ItemAction = Action<Item[]> | Action<Item> | Action<number>; // | Action<...>

export default function (state: ItemsState = initialState, action: ItemAction) {
  switch (action.type) {
    case GET_ITEMS: return {
      ...state,
      items: <Item[]>action.payload
    };
    case GET_ITEM: return {
      ...state,
      items: [...state.items, <Item>action.payload]
    };
    case UPDATE_ITEM: return {
      ...state,
      updatedItem: <Item>action.payload,
      items: state.items.map(item => item.id === (<Item>action.payload).id ? <Item>action.payload : item) // Immutable replace in array?
    };
    case DELETE_ITEM: return {
      ...state,
      items: state.items.filter(i => i.id !== <number>action.payload)
    }
    case CREATE_ITEM:
    default: return state;
  }
}