import { LOGIN, LOGOUT } from "../actions/Types";
import { Action } from ".";

export interface AuthState {
  isSignedIn: boolean|null;
  token?: string;
}

const initialState: AuthState = {
  isSignedIn: null,
  token: ""
};

type AuthAction = Action<string>;

export default function (state: AuthState = initialState, action: AuthAction) {
  switch (action.type) {
    case LOGIN: return {
      ...state,
      isSignedIn: true,
      token: <string>action.payload
    };
    case LOGOUT: return {
      ...state,
      isSignedIn: false,
      token: undefined
    };
    default: return state;
  }
}