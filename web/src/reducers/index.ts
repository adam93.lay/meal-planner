import { combineReducers } from 'redux';
import itemsReducer, { ItemsState } from './ItemsReducer';
import authReducer, { AuthState } from './AuthReducer';

export interface Action<P> {
  type: string;
  payload?: P;
}

export interface GlobalState {
  items: ItemsState;
  auth: AuthState;
}

export default combineReducers({
  items: itemsReducer,
  auth: authReducer
});
