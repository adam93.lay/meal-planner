
function authHeader() {
  const token = localStorage.getItem("auth_token");
  return {
    "Authorization": token ? ("Bearer " + token) : ""
  };
}

export function GET(url: string) {
  const token = localStorage.getItem("auth_token");

  return fetch(url, {
    headers: {
      ...authHeader()
    }
  })
}

export function POST(url: string, data: any) {
  return fetch(url, {
    method: "POST",
    headers: {
      ...authHeader(),
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  });
}

export function PUT(url: string, data: any) {
  return fetch(url, {
    method: "PUT",
    headers: {
      ...authHeader(),
      "Content-Type": "application/json"
    },
    body: JSON.stringify(data)
  });
}

export function DELETE(url: string) {
  return fetch(url, {
    method: "DELETE",
    headers: {
      ...authHeader()
    }
  });
}