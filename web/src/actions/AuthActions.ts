import { LOGIN, LOGOUT } from "./Types"
import { Dispatch } from "redux";
import { Action } from "../reducers";

export const signIn = (token: string) => (dispatch: Dispatch<Action<string>>) => {
  localStorage.setItem("auth_token", token);
  dispatch({
    type: LOGIN,
    payload: token
  });
};

export const signOut = () => (dispatch: Dispatch<any>) => {
  const auth = (window as any).gapi.auth2.getAuthInstance();
  auth
    .signOut()
    .then(() => dispatch({
      type: LOGOUT
    }));
};
