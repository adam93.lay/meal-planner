
export const LOGIN = "Login";
export const LOGOUT = "Logout";

export const GET_ITEMS = "Get items";
export const GET_ITEM = "Get single item";
export const CREATE_ITEM = "Set new item";
export const UPDATE_ITEM = "Update item";
export const DELETE_ITEM = "Delete item";
