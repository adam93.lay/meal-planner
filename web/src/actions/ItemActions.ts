import { GET_ITEMS, CREATE_ITEM, GET_ITEM, DELETE_ITEM, UPDATE_ITEM } from "./Types"
import Item from "../models/Item";
import { Dispatch } from "redux";
import { Action } from "../reducers";
import { GET, PUT, DELETE, POST } from "./Api";

export const getItems = () => (dispatch: Dispatch<Action<Item[]>>) => {
  GET('http://localhost:5000/api/items')
    .then(res => res.json())
    .then(items => dispatch({
      type: GET_ITEMS,
      payload: items
    })
  );
};

export const getItem = (id: number) => (dispatch: Dispatch<Action<Item[]>>) => {
  GET('http://localhost:5000/api/items/' + id)
    .then(res => res.json())
    .then(item => dispatch({
      type: GET_ITEM,
      payload: item
    })
  );
};

export const setItem = (newItem: Item) => (dispatch: Dispatch<any>) => {
  POST('http://localhost:5000/api/items', newItem)
    .then(res => res.json())
    .then(newItemId => {
      dispatch({
        type: CREATE_ITEM,
        payload: newItemId
      })
      dispatch(
        getItem(newItemId)
      );
    });
};

export const updateItem = (item: Item) => (dispatch: Dispatch<any>) => {
  PUT('http://localhost:5000/api/items/' + item.id, item)
    .then(res => res.json())
    .then(item => dispatch({
      type: UPDATE_ITEM,
      payload: item
    }));
};

export const deleteItem = (id: number) => (dispatch: Dispatch<any>) => {
  DELETE('http://localhost:5000/api/items/' + id)
    .then(() => {
      dispatch({
        type: DELETE_ITEM,
        payload: id
      })
    });
};
