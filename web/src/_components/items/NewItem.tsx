import React, { Component } from 'react'
import { setItem } from '../../actions/ItemActions';
import Item from '../../models/Item';
import { connect } from 'react-redux';
import { GlobalState } from '../../reducers';
import { Button, TextField, CardContent, Card } from '@material-ui/core';

interface NewItemProps {
  setItem: (newItem: Item) => void;
}

interface NewItemState {
  name: string;
}

class NewItem extends Component<NewItemProps, NewItemState> {

  constructor(props: NewItemProps) {
    super(props);
    this.state = {
      name: ""
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e: React.ChangeEvent<HTMLInputElement>) {
    
    this.setState({
      [e.target.name]: e.target.value
    } as any)
  }

  onSubmit(e: React.FormEvent<HTMLFormElement>) {

    e.preventDefault();

    const newItem = {
      name: this.state.name
    };

    this.props.setItem(newItem);
  }

  render() {
    return (
      <Card style={{marginBottom:24}}>
        <CardContent>
          <form onSubmit={this.onSubmit}>
            <TextField name="name" value={this.state.name} onChange={this.onChange} label="Name" />
            <br/>
            <br/>
            <Button type="submit" variant="contained" color="primary">Create!</Button>
          </form>
        </CardContent>
      </Card>
    );
  }
}

export default connect(null, { setItem })(NewItem);
