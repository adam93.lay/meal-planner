import React, { Component } from 'react'
import { updateItem, deleteItem } from '../../actions/ItemActions';
import Item from '../../models/Item';
import { connect } from 'react-redux';
import { GlobalState } from '../../reducers';
import { TableRow, TableCell, Button, TextField } from '@material-ui/core';
import { debounce } from 'throttle-debounce'

interface EditableItemRowProps {
  item: Item;

  updateItem: (item: Item) => void;
  deleteItem: (id: number) => void;
}

interface EditableItemRowState {
  name: string;
}

class EditableItemRow extends Component<EditableItemRowProps, EditableItemRowState> {

  constructor(props: EditableItemRowProps) {
    super(props);
    this.state = {
      name: props.item.name
    };

    this.updateItemDebounced = debounce(500, this.updateItem);
    this.onChange = this.onChange.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  updateItemDebounced: (state: EditableItemRowState) => void;
  updateItem() {
    const item = {
      id: this.props.item.id,
      name: this.state.name
    };
    this.props.updateItem(item);
  }

  onDelete() {
    this.props.deleteItem(this.props.item.id as number);
  }

  onChange(e: React.ChangeEvent<HTMLInputElement>) {
    
    // Set the state, then update the item automatically
    this.setState({ [e.target.name]: e.target.value } as any, () => {
      this.updateItemDebounced(this.state);
    });
  }

  render() {

    const { item } = this.props;

    return (
      <TableRow>
        <TableCell>{item.id}</TableCell>
        <TableCell>
          <TextField name="name" value={this.state.name} onChange={this.onChange} />
        </TableCell>
        <TableCell align="right">
          <Button variant="contained" color="secondary" onClick={this.onDelete}>Delete</Button>
        </TableCell>
      </TableRow>
    );
  }
}

export default connect(null, { updateItem, deleteItem })(EditableItemRow);
