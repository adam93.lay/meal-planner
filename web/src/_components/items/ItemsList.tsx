import React, { Component } from 'react'
import { getItems, deleteItem } from '../../actions/ItemActions';
import Item from '../../models/Item';
import { connect } from 'react-redux';
import { GlobalState } from '../../reducers';
import { Paper, Table, TableHead, TableRow, TableCell, TableBody, Button, TextField, Snackbar } from '@material-ui/core';
import EditableItemRow from './EditableItemRow';

interface ItemsListProps {
  items: Item[];
  updatedItem?: Item;

  getItems: () => void;
  deleteItem: (id: number) => void;
}

interface ItemsListState {
  successOpen: boolean;
}

class ItemsList extends Component<ItemsListProps, ItemsListState> {

  constructor(props: ItemsListProps) {
    super(props);
    this.state = {
      successOpen: false
    };
  }

  componentWillMount() {
    this.props.getItems();

    this.onDelete = this.onDelete.bind(this);
  }

  componentWillReceiveProps(nextProps: ItemsListProps) {
    // If received a new updated item...
    if (nextProps.updatedItem !== this.props.updatedItem) {
      // ...then trigger the alert
      this.setState({ successOpen: true });
    }
  }

  onDelete(id: number) {
    this.props.deleteItem(id);
  }

  render() {

    const { items } = this.props;

    return (
      <Paper>
        
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Id</TableCell>
              <TableCell>Name</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {items.map(i => <EditableItemRow key={i.id} item={i} />)}
          </TableBody>
        </Table>

        <Snackbar
          anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
          open={this.state.successOpen}
          autoHideDuration={5000}
          onClose={() => this.setState({ successOpen: false })}
          message={<span id="message-id">Item updated</span>} />

      </Paper>
    );
  }
}

const mapStateToProps = (state: GlobalState) => ({
  items: state.items.items,
  updatedItem: state.items.updatedItem
});

export default connect(mapStateToProps, { getItems, deleteItem })(ItemsList);
